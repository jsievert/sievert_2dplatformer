﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Runtime;

public class SceneController : MonoBehaviour
{
    private static string [] Levels = {"IntroScene", "GameOver", "TilemapStart", "EndScene"};
    private static int LevelIndex = 0;
    public float timer = 0f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        timer = timer + Time.deltaTime;
        if (timer > 68 && LevelIndex == 0){
            LevelIndex = 2;
            timer = 0;
            SceneManager.LoadScene(Levels[LevelIndex]);
        }
    }
    public static void Loss()
    {
        LevelIndex = 1;
        SceneManager.LoadScene(Levels[LevelIndex]);
    }
    public static void Finish()
    {
        LevelIndex = 3;
        SceneManager.LoadScene(Levels[LevelIndex]);
    }
}

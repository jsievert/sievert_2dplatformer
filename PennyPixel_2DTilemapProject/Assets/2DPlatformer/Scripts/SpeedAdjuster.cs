﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedAdjuster : MonoBehaviour
{
    float Level1 = 1000;
    float Level2 = 10000;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    public void SpeedCheck(float runTotal)
    {
        //increments tracker for player's time running
        PlayerPlatformerController.instance.runTime += 1;
        //sets player's speed equal to the appropriate amount for their level.
        if ( runTotal > Level2)
        {
            PlayerPlatformerController.instance.maxSpeed = 7f;
        }
        else if ( runTotal > Level1)
        {
            PlayerPlatformerController.instance.maxSpeed = 6f;
        }
    }
}

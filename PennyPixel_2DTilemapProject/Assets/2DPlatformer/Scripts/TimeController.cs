﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime;

public class TimeController : MonoBehaviour
{
    public float timer = 1800;
    // Start is called before the first frame update
    void Start()
    {
        timer = 0;
    }

    // Update is called once per frame
    public void timeTracker(bool closed)
    {
        //tracks time when menu is closed
        if (closed)
        {
            timer -= Time.deltaTime;
            if (timer < 0)
            {
                SceneController.Loss();
            }
        }
        else
        {
            //first call after the menu is closed from being open, we throwaway any time that was built up in deltaTime.
            float throwaway = Time.deltaTime;
        }
    }
    //translates our float into a nice XX:XX string format.
    public string timeReturn()
    {
        float minutes = Mathf.Floor(timer / 60);
        float seconds = Mathf.Floor(timer % 60);
        string time = minutes + ":" + seconds;
        return time;
    }
}

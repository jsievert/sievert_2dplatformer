﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpAdjuster : MonoBehaviour
{
    // Start is called before the first frame update
    private int Level1 = 10;
    private int Level2 = 100;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void JumpCheck(int jumpCount)
    {
        //increments jumps
        PlayerPlatformerController.instance.jumpsTaken += 1;
        if (jumpCount > Level2)
        {
            //increases jumpheight once and maintains that height for future jumps
            PlayerPlatformerController.instance.jumpTakeOffSpeed = 7.8f;
        }
        else if (jumpCount > Level1)
        {
            //increase jump height once and maintains that height for future jumps
            PlayerPlatformerController.instance.jumpTakeOffSpeed = 6.4f;
        }

    }
}

//HOW TO ACCESS PLAYER VARIABLES???

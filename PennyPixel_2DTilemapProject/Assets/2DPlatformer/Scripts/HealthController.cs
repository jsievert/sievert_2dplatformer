﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthController : MonoBehaviour
{
    //variables for each type of heart
    GameObject full1;
    GameObject full2;
    GameObject full3;
    GameObject half1;
    GameObject half2;
    GameObject half3;
    GameObject empty1;
    GameObject empty2;
    GameObject empty3;
    void Awake()
    {
        
    }

    // Update is called once per frame
    public void Overlay()
    {
        //we get all our instances from the player controller
       full1 = PlayerPlatformerController.instance.full1;
        full2 = PlayerPlatformerController.instance.full2;
        full3 = PlayerPlatformerController.instance.full3;
        half1 = PlayerPlatformerController.instance.half1;
        half2 = PlayerPlatformerController.instance.half2;
        half3 = PlayerPlatformerController.instance.half3;
        empty1 = PlayerPlatformerController.instance.empty1;
        empty2 = PlayerPlatformerController.instance.empty2;
        empty3 = PlayerPlatformerController.instance.empty3;
        //a series of if else statements for which hearts should be true
        //full hearts are on top of half which are on top of empty.
        //because of this, we only have to guarantee that the layers above and including the one we need are set properly,
        //and can ignore the others.
        //that's why this gets more complicated as it goes down.
        if (PlayerPlatformerController.instance.health == 6)
        {
            full1.SetActive(true);
            full2.SetActive(true);
            full3.SetActive(true);
        }
        else if (PlayerPlatformerController.instance.health == 5)
        {
            full1.SetActive(true);
            full2.SetActive(true);
            full3.SetActive(false);
            half3.SetActive(true);
        }
        else if (PlayerPlatformerController.instance.health == 4)
        {
            full1.SetActive(true);
            full2.SetActive(true);
            full3.SetActive(false);
            half3.SetActive(false);
            empty3.SetActive(true);
        }
        else if (PlayerPlatformerController.instance.health == 3)
        {
            full1.SetActive(true);
            full2.SetActive(false);
            half2.SetActive(true);
            full3.SetActive(false);
            half3.SetActive(false);
            empty3.SetActive(true); 
        }
        else if (PlayerPlatformerController.instance.health == 2)
        {
            full1.SetActive(true);
            full2.SetActive(false);
            half2.SetActive(false);
            empty2.SetActive(true);
            full3.SetActive(false);
            half3.SetActive(false);
            empty3.SetActive(true);
        }
        else if (PlayerPlatformerController.instance.health == 1)
        {
            full1.SetActive(false);
            half1.SetActive(true);
            full2.SetActive(false);
            half2.SetActive(false);
            empty2.SetActive(true);
            full3.SetActive(false);
            half3.SetActive(false);
            empty3.SetActive(true);
        }
        else if (PlayerPlatformerController.instance.health == 0)
        {
            full1.SetActive(false);
            half1.SetActive(false);
            empty1.SetActive(true);
            full2.SetActive(false);
            half2.SetActive(false);
            empty2.SetActive(true);
            full3.SetActive(false);
            half3.SetActive(false);
            empty3.SetActive(true);
            SceneController.Loss();
        }
    }
}

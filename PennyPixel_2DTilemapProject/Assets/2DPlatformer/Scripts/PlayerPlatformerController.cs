﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerPlatformerController : PhysicsObject
{
    //maxSpeed
    public float maxSpeed = 5;
    //supposedly affects speed of jumpTakeOff, but doesn't seem to make a difference.
    public float jumpTakeOffSpeed = 5f;
    //tracks amount of jumps.
    public int jumpsTaken = 0;
    public float runTime = 0;
    public bool endpoint = false;
    public GameObject menu;
    //we want this as an instance so other classes can adjust variables easily.
    public static PlayerPlatformerController instance;
    //component
    private SpriteRenderer spriteRenderer;
    //component
    private Animator animator;
    //will be a created script in the awake function.
    private JumpAdjuster jumpAdjuster;
    private SpeedAdjuster speedAdjuster;
    private TimeController timeController;
    private MenuController menuController;
    private HealthController healthController;
    public Text CoinText;
    public Text JumpLevelText;
    public Text SpeedLevelText;
    public Text TimerText;
    bool open = false;
    Vector2 shoplocation;
    Vector2 shopexit;
    bool shopentry = false;
    bool shopleave = false;
    int coins = 0;
    bool jumpreset = false;
    bool doublejump = true;
    bool doubleburn = false;
    bool damaged = false;
    public int health = 6;
    public int maxHealth = 6;
    public GameObject full1;
    public GameObject full2;
    public GameObject full3;
    public GameObject half1;
    public GameObject half2;
    public GameObject half3;
    public GameObject empty1;
    public GameObject empty2;
    public GameObject empty3;
    public GameObject upprompt;

    // Start is called before the first frame update
    void Awake()
    {
        //defining a lot of pieces we'll need later
        spriteRenderer =  GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();
        jumpAdjuster = new JumpAdjuster();
        speedAdjuster = new SpeedAdjuster();
        timeController = new TimeController();
        menuController = new MenuController();
        healthController = new HealthController();
        instance = this;
        shoplocation = new Vector2(2.5f,-7.5f);
        shopexit = new Vector2(8.5f,1f);
        CoinText.text = "Gold: 0";
    }
    protected override void InputCheck()
    {
        //checks if menu is open and will only decrease time when i'ts not open
        if (!open)
        {
            timeController.timeTracker(true);
        }
        else
        {
            timeController.timeTracker(false);
        }
        //when not in a menu and you press left or right movement keys, we'll track that.
        if (grounded && !open)
        {
            if (Input.GetKey("a")||Input.GetKey("d")||Input.GetKey(KeyCode.LeftArrow)||Input.GetKey(KeyCode.RightArrow))
            {
                speedAdjuster.SpeedCheck(runTime);
            }
        }
        //toggles menu
        if (Input.GetKeyDown(KeyCode.Return))
        {
            open = !open;
            if (open)
            {
                menu.SetActive(true);
                menuController.MenuToggle();
            }
            else
            {
                menu.SetActive(false);
            }
        }
        //if you are in the trigger for the shop, you can enter it.
        if (!open && shopentry)
        {
            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                EnterShop();
            }
        }
        //if you are in the trigger to leave the shop, you can exit it.
        if (!open && shopleave)
        {
            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                ExitShop();
            }
        }
        if (!open && endpoint)
        {
            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                SceneController.Finish();
            }
        }
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        //checks triggers
        if (other.gameObject.CompareTag ("ShopDoor"))
        {
            shopentry = true;
            //small pop up so player knows what button to press
            upprompt.SetActive(true);
        }
        if (other.gameObject.CompareTag ("ShopExit"))
        {
            shopleave = true;
            upprompt.SetActive(true);
        }
        if (other.gameObject.CompareTag("Coins"))
        {
            //picks up the object
            Pickup(other);
        }
        if (other.gameObject.CompareTag("Sage"))
        {
            endpoint = true;
            upprompt.SetActive(true);
        }
        if (other.gameObject.CompareTag("Damage"))
        {
            //damages the player
            health -= 1;
            //adjusts the health overlay
            healthController.Overlay();
            //sets this to true so we can repel player
            damaged = true;
            //turns off the damaged boolean after a half second
            Invoke("DamageReset",0.5f);
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        //turns off boolean triggers if they weren't already turned off by something else.
        if (other.gameObject.CompareTag ("ShopDoor"))
        {
            shopentry = false;
            upprompt.SetActive(false);
        }
        if (other.gameObject.CompareTag ("ShopExit"))
        {
            shopleave = false;
            upprompt.SetActive(false);
        }
        if (other.gameObject.CompareTag ("Sage"))
        {
            endpoint = false;
            upprompt.SetActive(false);
        }
    }

    protected override void ComputeVelocity()
    {
        //only works when the menu isn't open
        //otherwise pretty much same as tutorial
        if(!open)
        {
            Vector2 move = Vector2.zero;

            move.x = Input.GetAxis("Horizontal");

            if (Input.GetButtonDown("Jump")&& grounded)
            {
                velocity.y = jumpTakeOffSpeed;
                jumpAdjuster.JumpCheck(jumpsTaken);
                jumpreset = false;
                doubleburn = true;
            }
            else if (Input.GetButtonUp("Jump"))
            {
                jumpreset = true;
                if (velocity.y > 0)
                {
                    velocity.y = velocity.y * .5f;
                }
            }
            if (jumpreset && doublejump && Input.GetButtonDown("Jump") && doubleburn)
            {
                velocity.y = jumpTakeOffSpeed;
                jumpAdjuster.JumpCheck(jumpsTaken);
                doubleburn = false;
            }

            bool flipSprite = (spriteRenderer.flipX ? (move.x > 0.01f) : (move.x < 0.01f));
            if (flipSprite)
            {
                spriteRenderer.flipX = !spriteRenderer.flipX;
            }

            animator.SetBool("grounded", grounded);
            animator.SetFloat("velocityX", Mathf.Abs(velocity.x) / maxSpeed);
            if (damaged)
            {
                move.x = -move.x;
                move.y = -move.y;
            }

            targetVelocity = move * maxSpeed;
        }
        else
        {
            velocity.y = 0;
            velocity.x = 0;
        }

    }
    public void EnterShop()
    {
        //teleports player inside shop
        transform.position = shoplocation;
    }
    public void ExitShop()
    {
        //teleports player outside shop
        transform.position = shopexit;
    }
    public void Pickup(Collider2D coin)
    {
        //picks up object and adds one to player's currency.
        coin.gameObject.SetActive(false);
        coins += 1;
        CoinText.text = "Gems: " + coins;
    }
    public void DamageReset()
    {
        //makes it so we can stop sending the player away from a damaging trigger
        damaged = false;
    }
    public string GetTime()
    {
        //literally just gets the time
        return timeController.timeReturn();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuController : MonoBehaviour
{
    bool open;
    float jumpLength = 630f;
    float speedLength = 630f;
    RectTransform jumpBar;
    RectTransform speedBar;
    // Start is called before the first frame update
    void Awake()
    {
        open = false;
    }

    // Update is called once per frame
    public void MenuToggle()
    {
        float jumpsTaken = PlayerPlatformerController.instance.jumpsTaken;
        float runTime = PlayerPlatformerController.instance.runTime;
        //adjusts the length of xp bars to properly represent player progress.
        if (jumpsTaken < 10)
        {
            jumpLength = 630 - (jumpsTaken * 33);
        }
        else if (jumpsTaken < 100)
        {
            jumpLength = 630 - ((jumpsTaken-10) * (11 / 3));
            PlayerPlatformerController.instance.JumpLevelText.text = "Jump Level 1";
        }
        else
        {
            jumpLength = 300;
            PlayerPlatformerController.instance.JumpLevelText.text = "Jump Level 2";
        }
        if (runTime < 1000)
        {
            speedLength = 630 - (runTime * 1 / 3);
        }
        else if (runTime < 10000)
        {
            float ratio = 0.03666666666666666f;
            print(ratio);
            speedLength = 630 - ((runTime - 1000) * (ratio));
            print(speedLength);
            PlayerPlatformerController.instance.SpeedLevelText.text = "Speed Level 1";
        }
        else
        {
            speedLength = 300f;
            PlayerPlatformerController.instance.SpeedLevelText.text = "Speed Level 2";
        }
        PlayerPlatformerController.instance.TimerText.text = "Time Remaining:\n"+PlayerPlatformerController.instance.GetTime();
        jumpBar = GameObject.FindGameObjectWithTag("JumpFront").GetComponent<RectTransform>();
        jumpBar.offsetMax = new Vector2(-jumpLength, jumpBar.offsetMax.y);
        speedBar = GameObject.FindGameObjectWithTag("SpeedFront").GetComponent<RectTransform>();
        speedBar.offsetMax = new Vector2(-speedLength, speedBar.offsetMax.y);
    }
}
